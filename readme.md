# Similar Artists Engine

This is a simple web application that lets you search for an artist and
view similar artists based on genre.

## Requirements
* PHP >= 5.5.9
* Redis

## Installation
The application should work out of the box on any server with PHP > 5.5.9
and Redis. If you are using Valet or Homestead, simply follow the standard
configuration procedures.

If you're uploading to a server, upload to a web accessible directory and
point the document root to /public.

If you are running redis under a non-standard configuration, copy the .env.example
file to .env and populate the values to match your configuration.

## Rationale
The application is designed using Angular.js in the front-end and lumen in
the back-end. Since the purpose was simple, rather than building a full MVC
structure, I chose to set up the controller routes and use a service provider
to make the api queries and save the data.

## Roadmap

* Clean up the architecture and file structure
* Add unit and end-to-end tests