<?php

namespace App\Http\Controllers;

use App\ArtistSearch;
use Illuminate\Http\Request;

class ArtistController extends Controller
{
    public function search(ArtistSearch $artistSearch, Request $request)
    {
        $query = $request->input('query');
        $results = $artistSearch->search($query);
        return response()->json($results);
    }

    public function similarArtists(ArtistSearch $artistSearch, $id)
    {
        $results = $artistSearch->similarArtists($id);
        return response()->json($results);
    }
}
