<?php
/**
 * Created by PhpStorm.
 * User: daniel.wilhelmsen
 * Date: 5/24/16
 * Time: 12:38 PM
 */

namespace App;

use Underscore\Types\Arrays;

class ArtistSearch
{
    protected $redisConnection;

    public function redis()
    {
        return ($this->redisConnection) ?: new \Predis\Client([
            'scheme' => 'tcp',
            'host'   => env('REDIS_HOST', '127.0.0.1'),
            'port'   => env('REDIS_PORT', 6379),
            'database' => env('REDIS_DATABASE', 0),
            'password' => env('REDIS_PASSWORD', null),
        ]);
    }


    public function search($query)
    {
        $query = urlencode($query);
        $response = \Guzzle::get("https://api.spotify.com/v1/search?q=$query&type=artist");
        $queryResults = json_decode((string) $response->getBody(),true);

        $results = [];
        Arrays::each($queryResults['artists']['items'], function($artist) use (&$results) {
            $formattedResults = $this->formatResponse($artist);
            $this->redis()->hmset($artist['id'], [
                'name'      => $artist['name'],
                'genres'    => serialize($artist['genres']),
                'image'     => $formattedResults['image']
            ]);
            $results[] = $formattedResults;
        });
        return $results;
    }

    public function similarArtists($id)
    {
        $genres = $this->redis()->hmget($id, ['genres']);
        $genres = ($genres) ? unserialize($genres[0]) : null;
        $results = [];
        foreach($genres as $genre) {
            $query = urlencode($genre);
            $response = \Guzzle::get("https://api.spotify.com/v1/search?q=genre:%22$query%22&type=artist");
            $queryResults = json_decode((string) $response->getBody(),true);
            $results[$genre] = $this->bulkFormatResponse($queryResults['artists']['items']);
        }
        return $results;
    }

    private function formatResponse($artist)
    {
        $imageUrl = Arrays::first($artist['images'])['url'];
        return [
            'id'            => $artist['id'],
            'name'          => $artist['name'],
            'genres'        => $artist['genres'],
            'popularity'    => $artist['popularity'],
            'image'         => $imageUrl
        ];
    }

    private function bulkFormatResponse($artists)
    {
        return Arrays::each($artists, function($artist) {
            return $this->formatResponse($artist);
        });
    }
}