<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ArtistServiceProvider extends ServiceProvider
{
    protected $defer = true;
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\ArtistSearch', function(){

            return new \App\ArtistSearch();

        });
    }
}
