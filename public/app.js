'use strict';

// Declare app level module which depends on views, and components
angular.module('similar-artists-engine', [
  'ngRoute',
  'similar-artists-engine.home',
  'similar-artists-engine.results',
  'similar-artists-engine.searchbox-directive',
  'similar-artists-engine.similarartists-directive',
  'ui.bootstrap'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({
    templateUrl: 'app/home/home.html',
    controller: 'HomeCtrl'
  });
}]);