'use strict';

angular.module('similar-artists-engine.similarartists-directive', ['artistService'])
    .directive('similarartists', function($window,$interpolate, Artist, $location) {
        return {
            scope: {
                spotifyid: '@'
            },
            controller: function () {
                var vm = this;
                vm.simlarArtists = {};
                vm.initialized = false;

                vm.loadSimilarArtists = function(id) {
                    Artist.similarArtists(id)
                        .success(function(data){
                            vm.simlarArtists = data;
                            if(data.length == 0) {
                                vm.error = true;
                            }
                            vm.initialized = true;
                        })
                        .error(function(data) {
                            vm.error = true;
                        });
                };
            },
            controllerAs: 'similarartist',
            bindToController: true,
            templateUrl: 'app/components/similarartists/similarartists-directive.html'
        };
    });