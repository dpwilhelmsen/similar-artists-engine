'use strict';

angular.module('similar-artists-engine.searchbox-directive', ['ui.bootstrap','artistService'])
    .directive('searchbox', function($window, Artist, $location) {
        return {
            scope: {
                query: '@'
            },
            controller: function () {
                var vm = this;

                vm.query = '';
                vm.error = "Couldn't find any artists. Please check your query and try again.";
                vm.open= false;
                vm.toggleTooltip = function(action) {
                    vm.open = !vm.open;
                }
                vm.submit = function() {
                    var query = vm.query

                    //Query the artist
                    Artist.search(query)
                        .success(function(data) {
                            $location.path('/results/' + query);
                        })
                        .error(function(data) {
                            vm.toggleTooltip();
                        });
                }
            },
            controllerAs: 'search',
            bindToController: true,
            templateUrl: 'app/components/searchbox/searchbox-directive.html'
        };
    });
