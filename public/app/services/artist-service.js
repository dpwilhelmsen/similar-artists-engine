angular.module('artistService', [])

    .factory('Artist', function($http) {
        "use strict";
        var artistFactory = {};

        artistFactory.baseUrl = '/api/';

        //find artists
        artistFactory.search = function(query) {
            return $http.post(artistFactory.baseUrl + 'search',{query:query}, { cache: true});
        };

        //get similar artists
        artistFactory.similarArtists = function(id) {
            return $http.get(artistFactory.baseUrl + 'similar-artists/' + id, { cache: true});
        };

        return artistFactory;
    });