'use strict';

angular.module('similar-artists-engine.results', ['ngRoute', 'artistService'])

.config(['$routeProvider', function($routeProvider, Artist) {
  $routeProvider.when('/results/:query', {
    templateUrl: 'app/results/results.html',
    controller: 'ResultsCtrl',
    controllerAs: 'ctrl'
  });
}])

.controller('ResultsCtrl', ['$routeParams', 'Artist', function($routeParams, Artist) {
  var vm = this;

  vm.query = $routeParams.query;
  Artist.search(vm.query )
    .success(function(data){
      vm.results = data;
        if(data.length == 0) {
            vm.error = true;
        }
    })
    .error(function(data) {
      vm.error = true;
  });
}]);