<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" ng-app="similar-artists-engine" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" ng-app="similar-artists-engine" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" ng-app="similar-artists-engine" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" ng-app="similar-artists-engine" class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>The Similar Artist Engine</title>
    <base href="/">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootswatch/3.3.1/paper/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Bootstrap Material Design -->
    <link rel="stylesheet" type="text/css" href="app/lib/bootstrap-material-design/dist/css/bootstrap-material-design.css">
    <link rel="stylesheet" type="text/css" href="app/lib/bootstrap-material-design/dist/css/ripples.min.css">
    <link rel="stylesheet" href="app.css">
    <script src="app/lib/html5-boilerplate/dist/js/vendor/modernizr-2.8.3.min.js"></script>
</head>
<body>
<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">The Similar Artist Engine</a>
        </div>
    </div>
</nav>

<!-- Page Content -->
<div class="container">
    <div ng-view></div>

</div>

<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- In production use:
<script src="//ajax.googleapis.com/ajax/libs/angularjs/x.x.x/angular.min.js"></script>
-->
<script src="./app/lib/angular/angular.js"></script>
<script src="./app/lib/angular-route/angular-route.js"></script>
<script src="./app/lib/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
<script src="./app.js"></script>
<script src="./app/home/home.js"></script>
<script src="./app/results/results.js"></script>
<script src="./app/services/artist-service.js"></script>
<script src="./app/components/searchbox/searchbox-directive.js"></script>
<script src="./app/components/similarartists/similarartists-directive.js"></script>
</body>
</html>
